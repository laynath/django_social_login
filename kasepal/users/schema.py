import graphene
from django.contrib.auth import authenticate
from graphene_django import DjangoObjectType
from graphql_jwt.utils import jwt_encode, jwt_payload
from django.contrib.auth import get_user_model
from users.send_email import send_confirmation_email
import graphql_social_auth
from django.utils import timezone


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()


class Query(graphene.ObjectType):
    user = graphene.Field(UserType, id=graphene.UUID(required=True))
    me = graphene.Field(UserType)

    def resolve_user(self, info, id):
        return get_user_model().objects.get(id=id)

    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('User not logged in!')
        return user


class CreateUser(graphene.Mutation):
    """use to create user accout and email to confirm"""
    message = graphene.String()
    user = graphene.Field(UserType)

    class Arguments:
        username = graphene.String(required=True)
        email = graphene.String(required=True)
        password = graphene.String(required=True)

    def mutate(self, info, **kwargs):
        user = get_user_model().objects.create_user(
            email=kwargs.get('email'),
            username=kwargs.get('username'),
        )
        user.set_password(kwargs.get('password'))
        user.save()
        send_confirmation_email(email=user.email, username=user.username)
        return CreateUser(user=user, message="Successfully created user")


class LoginUser(graphene.Mutation):
    """use to login  and get token"""
    user = graphene.Field(UserType)
    message = graphene.String()
    token = graphene.String()
    verification_prompt = graphene.String()

    class Arguments:
        email = graphene.String()
        password = graphene.String()

    def mutate(self, info, **kwargs):
        email = kwargs.get('email')
        password = kwargs.get('password')
        user = authenticate(username=email, password=password)
        error_mg = 'Invalid login credentials'
        success_mg = "You logged in successfully."
        verification_error = 'Your email is not verified'
        if user:
            if user.is_verified:
                user.last_login = timezone.now()
                user.save(update_fields=['last_login'])
                payload = jwt_payload(user)
                token = jwt_encode(payload)
                return LoginUser(user=user,  token=token, message=success_mg)
            return LoginUser(message=verification_error)
        return LoginUser(message=error_mg)


class ChangePassword(graphene.Mutation):
    """use to change user password"""
    user = graphene.Field(UserType)

    class Arguments:
        id = graphene.ID()
        password = graphene.String()

    def mutate(self, info, id, password):
        user = info.context.user
        if user.is_anonymous:
            raise Exception("please logging to manage user details")
        else:
            user_obj = get_user_model().objects.get(pk=id)
            user_obj.set_password(password)
            user_obj.save()
        return ChangePassword(user=user)

        
class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    login_user = LoginUser.Field()
    change_password = ChangePassword.Field()
    social_auth = graphql_social_auth.SocialAuthJWT.Field()
