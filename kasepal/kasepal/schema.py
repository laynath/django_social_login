import graphene
import graphql_jwt
import users.schema as users
import profiles.schema as profiles
import uploads.schema as uploads
from users import patch


class Query(users.Query, 
            profiles.Query,
            uploads.Query,
            graphene.ObjectType
        ):
    pass

class Mutation(users.Mutation, 
               profiles.Mutation, 
               uploads.Mutation,
               graphene.ObjectType
        ):
    refresh_token = patch.Refresh.Field()
    verify_token = graphql_jwt.Verify.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)