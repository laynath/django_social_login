import graphene
from graphene_django import DjangoObjectType
from graphene_file_upload.scalars import Upload
from uploads.models import UploadImage


class ImageType(DjangoObjectType):
    class Meta:
        model = UploadImage


class Query(graphene.ObjectType):
    images = graphene.List(ImageType)

    def resolve_images(self, info):
        return UploadImage.objects.all()


class MyUpload(graphene.Mutation):
    class Arguments:
        files = Upload()
        
    ok = graphene.Boolean()

    @staticmethod
    def mutate(self, info, files=None):
        postimage = UploadImage(image=files)
        postimage.save()


            
        return MyUpload(ok=True)

class Mutation(graphene.ObjectType):
    my_upload = MyUpload.Field()