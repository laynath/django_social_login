import graphene
from graphene_django import DjangoObjectType
from profiles.models import UserProfile


class UserProfileType(DjangoObjectType):
    class Meta:
        model = UserProfile


class Query(graphene.ObjectType):
    userprofile = graphene.Field(
        UserProfileType, 
        id=graphene.UUID(required=True)
        )

    def resolve_userprofile(self, info, id):
        return UserProfile.objects.get(id=id)


class CreateProfile(graphene.Mutation):
    userprofile = graphene.Field(UserProfileType)

    class Arguments:
        usertype = graphene.String()
        avatar = graphene.String()
        gender = graphene.String()
        phone = graphene.String()
        dob = graphene.Date()
    
    def mutate(self, info, usertype, gender, phone, dob, avatar=None):
        user = info.context.user

        if user.is_anonymous:
            raise Exception('log in First')

        userprofile = UserProfile(
            user=user,
            usertype=usertype,
            avatar=avatar,
            gender=gender,
            phone = phone,
            dob=dob,
            )
        userprofile.save()
        return CreateProfile(userprofile=userprofile)

class Mutation(graphene.ObjectType):
    create_profile = CreateProfile.Field()

