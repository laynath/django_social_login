from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator
import uuid


class UserProfile(models.Model):
    id = models.UUIDField(
        primary_key=True, 
        default=uuid.uuid4, 
        editable=False
        )
    user = models.OneToOneField(
        get_user_model(),
        null=True,
        blank=True, 
        on_delete=models.CASCADE
        )
    TYPE_CHOICES = (
        ('B', 'Buyer'),
        ('S', 'Seller'),
        ('D', 'Delivery'),
    )
    usertype = models.CharField(
        max_length=1, 
        choices=TYPE_CHOICES, 
        blank=True
    )
    avatar = models.ImageField(
        upload_to='avatars',
        blank=True, 
        null=True,
        )
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('Other', 'Other'),
    )
    gender = models.CharField(
        max_length=1, 
        choices=GENDER_CHOICES, 
        blank=True
        )
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$', 
        message="Phone number must be entered in the format: '+999999999'.")
    phone = models.CharField(
        validators=[phone_regex], 
        max_length=20, blank=True)
    dob = models.DateField(null=True)

